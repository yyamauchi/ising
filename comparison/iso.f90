program thermal

    integer, parameter :: Nsample = 50000, Nskip=2

    character(len=50) :: Lstr, Jstr
    integer :: L
    real :: J, p
    real :: samples(1,Nsample)

    if (command_argument_count() /= 2) then
        stop "L J"
    end if

    call get_command_argument(1, Lstr)
    call get_command_argument(2, Jstr)

    read (Lstr,*) L
    read (Jstr,*) J

    ! Probability of adding to cluster is (1 - exp(-2 J)).
    p = 1 - exp(-2*J)

    call main

contains

subroutine main
    logical :: lattice(L,L,L)
    integer sample, cluster

    lattice = .false.

    do sample = 1,Nsample
        do cluster = 1,Nskip
            call update(lattice)
        end do
        call measure(lattice,sample)
    end do
end subroutine

subroutine update(lattice)
    logical, intent(inout) :: lattice(L,L,L)
    logical :: links(3,L,L,L)
    logical :: done(L,L,L)
    logical :: spin
    integer :: x, y, t, xp, yp, tp
    real :: u

    ! Select links.
    links = .false.
    do x = 1,L
        xp = mod(x,L)+1
        do y = 1,L
            yp = mod(y,L)+1
            do t = 1,L
                tp = mod(t,L)+1
                if (lattice(x,y,t) .eqv. lattice(xp,y,t)) then
                    call random_number(u)
                    if (u < p) then
                        links(1,x,y,t) = .true.
                    end if
                end if
                if (lattice(x,y,t) .eqv. lattice(x,yp,t)) then
                    call random_number(u)
                    if (u < p) then
                        links(2,x,y,t) = .true.
                    end if
                end if
                if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                    call random_number(u)
                    if (u < p) then
                        links(3,x,y,t) = .true.
                    end if
                end if
            end do
        end do
    end do

    ! Flip clusters.
    done = .false.
    do x = 1,L
        do y = 1,L
            do t = 1,L
                if (done(x,y,t)) then
                    cycle
                end if
                call random_number(u)
                call flood_flip(links,lattice,done,x,y,t,u < 0.5)
            end do
        end do
    end do
end subroutine

subroutine flood_flip(links,lattice,done,x0,y0,t0,spin)
    logical, intent(in) :: links(3,L,L,L)
    logical, intent(inout), dimension(L,L,L) :: lattice, done
    integer, intent(in) :: x0, y0, t0
    logical :: spin
    integer :: x,y,t,xp,yp,tp
    integer :: stack(3,L*L*L), top

    stack(:,1) = (/x0,y0,t0/)
    top = 1
    done(x0,y0,t0) = .true.

    do while (top > 0)
        ! Pop a vertex off the stack.
        x = stack(1,top)
        y = stack(2,top)
        t = stack(3,top)
        top = top-1

        ! Flip it.
        lattice(x,y,t) = spin

        ! Push all adjacent, non-done vertices.
        xp = mod(x,L)+1
        yp = mod(y,L)+1
        tp = mod(t,L)+1
        if (links(1,x,y,t) .and. .not. done(xp,y,t)) then
            call push_mark(stack,top,done,xp,y,t)
        end if
        if (links(2,x,y,t) .and. .not. done(x,yp,t)) then
            call push_mark(stack,top,done,x,yp,t)
        end if
        if (links(3,x,y,t) .and. .not. done(x,y,tp)) then
            call push_mark(stack,top,done,x,y,tp)
        end if

        xp = mod(L+x-2,L)+1
        yp = mod(L+y-2,L)+1
        tp = mod(L+t-2,L)+1
        if (links(1,xp,y,t) .and. .not. done(xp,y,t)) then
            call push_mark(stack,top,done,xp,y,t)
        end if
        if (links(2,x,yp,t) .and. .not. done(x,yp,t)) then
            call push_mark(stack,top,done,x,yp,t)
        end if
        if (links(3,x,y,tp) .and. .not. done(x,y,tp)) then
            call push_mark(stack,top,done,x,y,tp)
        end if
    end do
end subroutine

subroutine push_mark(stack,top,done,x,y,t)
    integer, intent(inout) :: stack(3,L*L*L), top
    logical, intent(inout) :: done(L,L,L)
    integer :: x,y,t
    top = top+1
    stack(1,top) = x
    stack(2,top) = y
    stack(3,top) = t
    done(x,y,t) = .true.
end subroutine

subroutine measure(lattice,N)
    logical, intent(inout) :: lattice(L,L,L)
    integer, intent(in) :: N

    if (.false.) then
        if (lattice(1,1,1) .eqv. lattice(2,1,1)) then
            samples(1,N) = 1
        else
            samples(1,N) = -1
        end if
    end if

    samples(1,N) = (real(count(lattice))/(L*L*L))**2

    if (mod(N,100) == 0) then
        print *, sum(samples(1,:N))/real(N)
    end if
end subroutine

end program
