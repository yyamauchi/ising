program thermal

    integer, parameter :: Nsample = 1000000, Nskip=100

    character(len=50) :: Lxstr, Lystr, mustr, betastr, Nbetastr
    integer :: Lx, Ly, Nbeta
    real :: mu, beta, delta
    real :: J_space, J_time
    real :: samples(1,Nsample)

    if (command_argument_count() /= 5) then
        stop "Lx Ly mu beta Nbeta"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, mustr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, Nbetastr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (mustr,*) mu
    read (betastr,*) beta
    read (Nbetastr,*) Nbeta

    delta = beta/Nbeta
    J_space = delta
    J_time = -log(tanh(delta*mu))/2

    call main

contains

subroutine main
    logical :: lattice(Lx,Ly,Nbeta)
    integer sample, cluster

    block
        integer :: x, y, t
        real :: u
        do x = 1,Lx
            do y = 1,Ly
                do t = 1,Nbeta
                    call random_number(u)
                    lattice(x,y,t) = u < 0.5
                end do
            end do
        end do
    end block

    do sample = 1,Nsample
        do cluster = 1,Nskip
            call update(lattice)
        end do
        call measure(lattice,sample)
    end do
end subroutine

subroutine update(lattice)
    logical, intent(inout) :: lattice(Lx,Ly,Nbeta)
    integer :: x, y, t, xp, yp, tp, xm, ym, tm
    real :: Sp, Sm
    real :: u

    do x = 1,Lx
        do y = 1,Ly
            do t = 1,Nbeta
                xp = mod(x,Lx)+1
                yp = mod(y,Ly)+1
                tp = mod(t,Nbeta)+1

                xm = mod(x-2+Lx,Lx)+1
                ym = mod(y-2+Ly,Ly)+1
                tm = mod(t-2+Nbeta,Nbeta)+1

                Sp = 0
                Sp = Sp - J_space * spin(.true. .eqv. lattice(xp,y,t))
                Sp = Sp - J_space * spin(.true. .eqv. lattice(x,yp,t))
                Sp = Sp - J_time * spin(.true. .eqv. lattice(x,y,tp))
                Sp = Sp - J_space * spin(.true. .eqv. lattice(xm,y,t))
                Sp = Sp - J_space * spin(.true. .eqv. lattice(x,ym,t))
                Sp = Sp - J_time * spin(.true. .eqv. lattice(x,y,tm))

                Sm = 0
                Sm = Sm - J_space * spin(.false. .eqv. lattice(xp,y,t))
                Sm = Sm - J_space * spin(.false. .eqv. lattice(x,yp,t))
                Sm = Sm - J_time * spin(.false. .eqv. lattice(x,y,tp))
                Sm = Sm - J_space * spin(.false. .eqv. lattice(xm,y,t))
                Sm = Sm - J_space * spin(.false. .eqv. lattice(x,ym,t))
                Sm = Sm - J_time * spin(.false. .eqv. lattice(x,y,tm))

                call random_number(u)
                if (u < exp(Sp-Sm)) then
                    lattice(x,y,t) = .false.
                else
                    lattice(x,y,t) = .true.
                end if
            end do
        end do
    end do
end subroutine

pure elemental function spin(l) result(s)
    logical, intent(in) :: l
    integer s
    if (l) then
        s = 1
    else
        s = -1
    end if
end function

subroutine measure(lattice,N)
    logical, intent(inout) :: lattice(Lx,Ly,Nbeta)
    integer, intent(in) :: N

    if (.false.) then
        samples(1,N) = (real(count(lattice))/(Lx*Ly*Nbeta))**2
    end if

    block
        real :: H
        integer :: x,y,t,xp,yp,tp
        logical :: same
        real :: s, cdm, sdm, pot

        cdm = cosh(delta*mu)
        sdm = sinh(delta*mu)

        H = 0

        do x = 1,Lx
            do y = 1,Ly
                do t = 1,Nbeta
                    pot = 0
                    xp = mod(x,Lx)+1
                    yp = mod(y,Ly)+1
                    tp = mod(t,Nbeta)+1

                    if (xp /= x) then
                        same = lattice(x,y,t) .eqv. lattice(xp,y,t)
                        pot = pot - spin(same)
                    end if

                    if (yp /= y) then
                        same = lattice(x,y,t) .eqv. lattice(x,yp,t)
                        pot = pot - spin(same)
                    end if

                    H = H + pot

                    if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                        H = H - mu*sdm/cdm * exp(delta*pot*2)
                    else
                        H = H - mu*cdm/sdm * exp(delta*pot*2)
                    end if
                end do
            end do
        end do

        samples(1,N) = H/Nbeta
        print *, H/Nbeta
    end block

    if (.false. .and. mod(N,1000) == 0) then
        print *, sum(samples(1,:N))/real(N)
    end if
end subroutine

end program
