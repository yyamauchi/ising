#!/usr/bin/env python

import sys
from numpy import *
import numpy.random as random

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])
mu = float(sys.argv[3])
J = 1.

pauli_x = array([[0,1],[1,0]])
pauli_z = array([[1,0],[0,-1]])

sigma_x = [eye(1)]*Lx*Ly
sigma_z = [eye(1)]*Lx*Ly

for n in range(Lx*Ly):
    for m in range(Lx*Ly):
        if n == m:
            sigma_x[n] = kron(sigma_x[n], pauli_x)
            sigma_z[n] = kron(sigma_z[n], pauli_z)
        else:
            sigma_x[n] = kron(sigma_x[n], eye(2))
            sigma_z[n] = kron(sigma_z[n], eye(2))

dim = sigma_x[0].shape[0]
H = zeros(sigma_x[0].shape)
W = zeros(sigma_x[0].shape, dtype=complex128) # The wave goes in the x-direction.

for x in range(Lx):
    for y in range(Ly):
        n = y*Lx+x
        H -= mu*sigma_x[n]
        W -= mu*sigma_x[n] * exp(1j*2*pi*x/Lx)
        xp = (x+1)%Lx
        yp = y
        np = yp*Lx+xp
        H -= J*sigma_z[n]@sigma_z[np]
        W -= J*sigma_z[n]@sigma_z[np] * exp(1j*2*pi*(x+0.5)/Lx)
        xp = x
        yp = (y+1)%Ly
        np = yp*Lx+xp
        H -= J*sigma_z[n]@sigma_z[np]
        W -= J*sigma_z[n]@sigma_z[np] * exp(1j*2*pi*x/Lx)

vals, vecs = linalg.eigh(H)

print(vals[1] - vals[0], vals[2] - vals[0])


if False:
    mins = []
    maxs = []
    #compute gap ratio
    for i in range(dim-2):
        mins.append(min(vals[i+1]-vals[i],vals[i+2]-vals[i+1]))
        maxs.append(max(vals[i+1]-vals[i],vals[i+2]-vals[i+1]))
    mins = array(mins)
    maxs = array(maxs)
    print(mean(mins/maxs))

import matplotlib.pyplot as plt

plt.hist(vals,bins=40)
plt.show()
