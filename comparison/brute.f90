program thermal

    real, parameter :: PI = 3.14159265358979323846

    character(len=50) :: Lxstr, Lystr, mustr, betastr, Nbetastr
    integer :: Lx, Ly, Nbeta
    real :: mu, beta, delta
    real :: J_space, p_space, p_time

    if (command_argument_count() /= 5) then
        stop "Lx Ly mu beta Nbeta"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, mustr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, Nbetastr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (mustr,*) mu
    read (betastr,*) beta
    read (Nbetastr,*) Nbeta

    delta = beta/Nbeta
    J_space = delta

    call main

contains

subroutine main
    logical :: lattice(Lx,Ly,Nbeta)
    integer x,y,t,i
    integer config
    real numerator, denominator, S, cor(Nbeta-1)

    cor = 0
    numerator = 0
    denominator = 0
    do config = 0,2**(Lx*Ly*Nbeta)-1
        ! Create lattice.
        do x = 0,Lx-1
            do y = 0,Ly-1
                do t = 0,Nbeta-1
                    i = x + y*Lx + t*Lx*Ly
                    lattice(x+1,y+1,t+1) = btest(config,i)
                end do
            end do
        end do

        ! Measure action.
        S = act(lattice)

        ! Measure hamiltonian
        numerator = numerator + hamiltonian(lattice)*exp(-S)
        ! Correlator
        do t = 2,Nbeta
            cor(t-1) = cor(t-1) + exp(-S)*wave(lattice,1)*wave(lattice,t)
        end do
        ! Boltzmann factor
        denominator = denominator + exp(-S)
    end do

    print *, numerator/denominator, cor/denominator
end subroutine

pure elemental function spin(l) result(s)
    logical, intent(in) :: l
    integer s
    if (l) then
        s = 1
    else
        s = -1
    end if
end function

function act(lattice) result(S)
    logical, intent(in) :: lattice(Lx,Ly,Nbeta)
    real :: S
    integer x,y,t,xp,yp,tp
    S = 0
    do x = 1,Lx
        do y = 1,Ly
            do t = 1,Nbeta
                xp = mod(x,Lx)+1
                yp = mod(y,Ly)+1
                tp = mod(t,Nbeta)+1

                if (xp /= x) then
                    S = S - J_space*spin(lattice(x,y,t) .eqv. lattice(xp,y,t))
                end if
                if (yp /= y) then
                    S = S - J_space*spin(lattice(x,y,t) .eqv. lattice(x,yp,t))
                end if
                if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                    S = S - log(cosh(delta*mu))
                else
                    S = S - log(sinh(delta*mu))
                endif
            end do
        end do
    end do
end function

function hamiltonian(lattice) result(H)
    logical, intent(in) :: lattice(Lx,Ly,Nbeta)
    real :: H
    integer :: x,y,t,xp,yp,tp
    logical :: same
    real :: s, cdm, sdm, pot

    cdm = cosh(delta*mu)
    sdm = sinh(delta*mu)

    H = 0

    do x = 1,Lx
        do y = 1,Ly
            do t = 1,Nbeta
                pot = 0
                xp = mod(x,Lx)+1
                yp = mod(y,Ly)+1
                tp = mod(t,Nbeta)+1

                if (xp /= x) then
                    same = lattice(x,y,t) .eqv. lattice(xp,y,t)
                    pot = pot - spin(same)
                end if

                if (yp /= y) then
                    same = lattice(x,y,t) .eqv. lattice(x,yp,t)
                    pot = pot - spin(same)
                end if

                H = H + pot

                if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                    H = H - mu*sdm/cdm * exp(delta*pot*2)
                else
                    H = H - mu*cdm/sdm * exp(delta*pot*2)
                end if
            end do
        end do
    end do

    H = H/Nbeta
end function

function wave(lattice, t) result(W)
    logical, intent(in) :: lattice(Lx,Ly,Nbeta)
    integer, intent(in) :: t
    real :: W
    integer :: x,y,xp,yp,tp,xm,ym,tm
    logical :: same
    real :: s, cdm, sdm, pot

    cdm = cosh(delta*mu)
    sdm = sinh(delta*mu)

    W = 0

    do x = 1,Lx
        do y = 1,Ly
            pot = 0

            xp = mod(x,Lx)+1
            yp = mod(y,Ly)+1
            tp = mod(t,Nbeta)+1

            xm = mod(x-2+Lx,Lx)+1
            ym = mod(y-2+Ly,Ly)+1
            tm = mod(t-2+Nbeta,Nbeta)+1

            if (xp /= x) then
                same = lattice(x,y,t) .eqv. lattice(xp,y,t)
                pot = pot - spin(same)
                W = W - spin(same) * cos(2.*PI/Lx * (x+0.5))
            end if

            if (yp /= y) then
                same = lattice(x,y,t) .eqv. lattice(x,yp,t)
                pot = pot - spin(same)
                W = W - spin(same) * cos(2.*PI/Lx * x)
            end if

            if (xm /= x) then
                same = lattice(x,y,t) .eqv. lattice(xm,y,t)
                pot = pot - spin(same)
            end if

            if (ym /= y) then
                same = lattice(x,y,t) .eqv. lattice(x,ym,t)
                pot = pot - spin(same)
            end if

            if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                W = W - mu*sdm/cdm * exp(delta*pot) * cos(2.*PI/Lx * x)
            else
                W = W - mu*cdm/sdm * exp(delta*pot) * cos(2.*PI/Lx * x)
            end if
        end do
    end do
end function

end program
