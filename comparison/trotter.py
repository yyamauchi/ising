#!/usr/bin/env python

import sys
from numpy import *
import numpy.random as random

Lx = int(sys.argv[1])
Ly = int(sys.argv[2])
mu = float(sys.argv[3])
J = 1.
beta = float(sys.argv[4])
Nbeta = int(sys.argv[5])

pauli_x = array([[0,1],[1,0]])
pauli_z = array([[1,0],[0,-1]])

sigma_x = [eye(1)]*Lx*Ly
sigma_z = [eye(1)]*Lx*Ly

for n in range(Lx*Ly):
    for m in range(Lx*Ly):
        if n == m:
            sigma_x[n] = kron(sigma_x[n], pauli_x)
            sigma_z[n] = kron(sigma_z[n], pauli_z)
        else:
            sigma_x[n] = kron(sigma_x[n], eye(2))
            sigma_z[n] = kron(sigma_z[n], eye(2))

Hx = zeros(sigma_x[0].shape)
Hz = zeros(sigma_x[0].shape)
W = zeros(sigma_x[0].shape)

D = Hx.shape[0]

for x in range(Lx):
    for y in range(Ly):
        n = y*Lx+x
        Hx -= mu*sigma_x[n]
        W -= mu*sigma_x[n] * cos(2*pi*x/Lx)
        xp = (x+1)%Lx
        yp = y
        np = yp*Lx+xp
        if n != np:
            Hz -= J*sigma_z[n]@sigma_z[np]
            W -= J*sigma_z[n]@sigma_z[np] * cos(2*pi*(x+0.5)/Lx)
        xp = x
        yp = (y+1)%Ly
        np = yp*Lx+xp
        if n != np:
            Hz -= J*sigma_z[n]@sigma_z[np]
            W -= J*sigma_z[n]@sigma_z[np] * cos(2*pi*x/Lx)

xvals, xvecs = linalg.eigh(Hx)
zvals, zvecs = linalg.eigh(Hz)
xrho = xvecs @ diag(exp(-beta/Nbeta*xvals)) @ xvecs.conj().T
zrho = zvecs @ diag(exp(-beta/Nbeta*zvals/2)) @ zvecs.conj().T

for dt in range(Nbeta):
    rho1 = eye(Hx.shape[0])
    rho2 = eye(Hx.shape[0])
    for _ in range(dt):
        rho1 = rho1 @ zrho
        rho1 = rho1 @ xrho
        rho1 = rho1 @ zrho
    for _ in range(Nbeta-dt):
        rho2 = rho2 @ zrho
        rho2 = rho2 @ xrho
        rho2 = rho2 @ zrho


    print(dt, (rho1 @ W @ rho2 @ W).trace() / (rho1 @ rho2).trace()) # , (rho1 @ rho2 @ (Hx+Hz)).trace() / (rho1 @ rho2).trace())

