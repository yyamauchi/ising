#!/usr/bin/env python

import numpy as np
import sys

import matplotlib.pyplot as plt

filename = sys.argv[1]

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])
print(dat.shape)

decay = 3/max(dat[:,0])

@np.vectorize
def fourier(omega):
    return np.trapz(dat[:,1]*np.exp(1j*omega*dat[:,0])*np.exp(-decay*dat[:,0]), dat[:,0])/len(dat)

omega = np.linspace(0,20,5000)
amp = fourier(omega)

f = open(filename, 'w')
for n in range(5000):
	f.write(str(omega[n]))
	f.write(" ")
	f.write(str(amp[n].real))
	f.write(" ")
	f.write(str(amp[n].imag))
	f.write("\n")
f.close()


plt.plot(omega,amp.real)
plt.plot(omega,amp.imag)
plt.show()


