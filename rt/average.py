#!/usr/bin/env python

from glob import glob
import statistics
import sys

import numpy as np

def bootstrap(x, w=None, N=100):
    if w is None:
        w = x*0 + 1
    y = x * w
    m = (sum(y) / sum(w)).real
    ms = []
    for n in range(N):
        s = np.random.choice(range(len(x)), len(x))
        ms.append((sum(y[s]) / sum(w[s])).real)
    return m, np.std(ms)

prefix = sys.argv[1]

dat = []
for fn in glob(f'{prefix}.*'):
    with open(fn) as f:
        dat.append([[float(x) for x in l.split()] for l in f.readlines()])

# Get the mode length, and drop all else.
length = statistics.mode(map(len,dat))
dat = np.array(list(filter(lambda d: len(d)==length,dat)))

# TODO dat[:,t,2] is lognorm, not a weight

time = dat[0,:,0]
cor = np.zeros(length)
err = np.zeros(length)
wts = np.zeros(length)
for t in range(length):
    cor[t], err[t] = bootstrap(dat[:,t,1], np.exp(dat[:,t,2]))
    wts[t] = np.log(np.sum(np.exp(dat[:,t,2])))
    print(dat[0,t,0], cor[t], wts[t], err[t])
