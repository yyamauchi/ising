#!/bin/sh

set -e

prefix=$1
shift

mkdir -p data
for n in `seq 1 100000`; do
	echo "rt/rt $* > data/$prefix.$n"
done

