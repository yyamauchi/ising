#!/usr/bin/env python

import numpy as np
import sys

import matplotlib.pyplot as plt

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])
print(dat.shape)

decay = max(dat[:,0])/4

@np.vectorize
def fourier(omega):
    return np.trapz(dat[:,1]*np.exp(1j*omega*dat[:,0])*np.exp(-(dat[:,0]/decay)**2), dat[:,0])/len(dat)

omega = np.linspace(0,10,5000)
amp = fourier(omega)

plt.plot(omega,amp.real)
#plt.plot(omega,amp.imag)
plt.show()
