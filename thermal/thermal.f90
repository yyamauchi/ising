program thermal

    real, parameter :: PI = 3.14159265358979323846

    integer, parameter :: Nskip = 5

    character(len=50) :: Lxstr, Lystr, mustr, betastr, Nbetastr, Nsamplestr, Nbatchstr
    integer :: Nsample, Nbatch
    integer :: Lx, Ly, Nbeta
    real :: mu, beta, delta
    real :: J_space, J_time, p_space, p_time
    real, allocatable :: samples(:,:)

    if (command_argument_count() /= 7) then
        stop "Lx Ly mu beta Nbeta Nsample Nbatch"
    end if

    call get_command_argument(1, Lxstr)
    call get_command_argument(2, Lystr)
    call get_command_argument(3, mustr)
    call get_command_argument(4, betastr)
    call get_command_argument(5, Nbetastr)
    call get_command_argument(6, Nsamplestr)
    call get_command_argument(7, Nbatchstr)

    read (Lxstr,*) Lx
    read (Lystr,*) Ly
    read (mustr,*) mu
    read (betastr,*) beta
    read (Nbetastr,*) Nbeta
    read (Nsamplestr,*) Nsample
    read (Nbatchstr,*) Nbatch

    delta = beta/Nbeta
    J_space = delta
    J_time = -log(tanh(delta*mu))/2

    ! Probability of adding to cluster is (1 - exp(-2 J)).
    p_space = 1 - exp(-2*J_space)
    p_time = 1 - exp(-2*J_time)

    allocate(samples(Nbeta,Nbatch))

    call main

    deallocate(samples)

contains

subroutine main
    logical :: lattice(Lx,Ly,Nbeta)
    integer sample, cluster, batch

    lattice = .false.

    do sample = 1,Nsample
        do batch = 1,Nbatch
            do cluster = 1,Nskip
                call update(lattice)
            end do
            call measure(lattice, samples(:,batch))
        end do
        print *, sum(samples(:,:), dim=2)/Nbatch
    end do
end subroutine

subroutine update(lattice)
    logical, intent(inout) :: lattice(Lx,Ly,Nbeta)
    logical :: links(3,Lx,Ly,Nbeta)
    logical :: done(Lx,Ly,Nbeta)
    logical :: spin
    integer :: x, y, t, xp, yp, tp
    real :: u

    ! Select links.
    links = .false.
    do x = 1,Lx
        xp = mod(x,Lx)+1
        do y = 1,Ly
            yp = mod(y,Ly)+1
            do t = 1,Nbeta
                tp = mod(t,Nbeta)+1
                if (lattice(x,y,t) .eqv. lattice(xp,y,t)) then
                    call random_number(u)
                    if (u < p_space) then
                        links(1,x,y,t) = .true.
                    end if
                end if
                if (lattice(x,y,t) .eqv. lattice(x,yp,t)) then
                    call random_number(u)
                    if (u < p_space) then
                        links(2,x,y,t) = .true.
                    end if
                end if
                if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                    call random_number(u)
                    if (u < p_time) then
                        links(3,x,y,t) = .true.
                    end if
                end if
            end do
        end do
    end do

    ! Flip clusters.
    done = .false.
    do x = 1,Lx
        do y = 1,Ly
            do t = 1,Nbeta
                if (done(x,y,t)) then
                    cycle
                end if
                call random_number(u)
                call flood_flip(links,lattice,done,x,y,t,u < 0.5)
            end do
        end do
    end do
end subroutine

subroutine flood_flip(links,lattice,done,x0,y0,t0,spin)
    logical, intent(in) :: links(3,Lx,Ly,Nbeta)
    logical, intent(inout), dimension(Lx,Ly,Nbeta) :: lattice, done
    integer, intent(in) :: x0, y0, t0
    logical :: spin
    integer :: x,y,t,xp,yp,tp
    integer :: stack(3,Lx*Ly*Nbeta), top

    stack(:,1) = (/x0,y0,t0/)
    top = 1
    done(x0,y0,t0) = .true.

    do while (top > 0)
        ! Pop a vertex off the stack.
        x = stack(1,top)
        y = stack(2,top)
        t = stack(3,top)
        top = top-1

        ! Flip it.
        lattice(x,y,t) = spin

        ! Push all adjacent, non-done vertices.
        xp = mod(x,Lx)+1
        yp = mod(y,Ly)+1
        tp = mod(t,Nbeta)+1
        if (links(1,x,y,t) .and. .not. done(xp,y,t)) then
            call push_mark(stack,top,done,xp,y,t)
        end if
        if (links(2,x,y,t) .and. .not. done(x,yp,t)) then
            call push_mark(stack,top,done,x,yp,t)
        end if
        if (links(3,x,y,t) .and. .not. done(x,y,tp)) then
            call push_mark(stack,top,done,x,y,tp)
        end if

        xp = mod(Lx+x-2,Lx)+1
        yp = mod(Ly+y-2,Ly)+1
        tp = mod(Nbeta+t-2,Nbeta)+1
        if (links(1,xp,y,t) .and. .not. done(xp,y,t)) then
            call push_mark(stack,top,done,xp,y,t)
        end if
        if (links(2,x,yp,t) .and. .not. done(x,yp,t)) then
            call push_mark(stack,top,done,x,yp,t)
        end if
        if (links(3,x,y,tp) .and. .not. done(x,y,tp)) then
            call push_mark(stack,top,done,x,y,tp)
        end if
    end do
end subroutine

subroutine push_mark(stack,top,done,x,y,t)
    integer, intent(inout) :: stack(3,Lx*Ly*Nbeta), top
    logical, intent(inout) :: done(Lx,Ly,Nbeta)
    integer :: x,y,t
    top = top+1
    stack(:,top) = (/ x,y,t /)
    done(x,y,t) = .true.
end subroutine

pure elemental function spin(l) result(s)
    logical, intent(in) :: l
    integer s
    if (l) then
        s = 1
    else
        s = -1
    end if
end function

function hamiltonian(lattice, t) result(H)
    logical, intent(in) :: lattice(Lx,Ly,Nbeta)
    integer, intent(in) :: t
    real :: H
    integer :: x,y,xp,yp,tp
    logical :: same
    real :: s, cdm, sdm, pot

    cdm = cosh(delta*mu)
    sdm = sinh(delta*mu)

    H = 0

    do x = 1,Lx
        do y = 1,Ly
            pot = 0
            xp = mod(x,Lx)+1
            yp = mod(y,Ly)+1
            tp = mod(t,Nbeta)+1

            if (xp /= x) then
                same = lattice(x,y,t) .eqv. lattice(xp,y,t)
                pot = pot - spin(same)
            end if

            if (yp /= y) then
                same = lattice(x,y,t) .eqv. lattice(x,yp,t)
                pot = pot - spin(same)
            end if

            H = H + pot

            if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                H = H - mu*sdm/cdm * exp(delta*pot*2)
            else
                H = H - mu*cdm/sdm * exp(delta*pot*2)
            end if
        end do
    end do
end function

function wave(lattice, t) result(W)
    logical, intent(in) :: lattice(Lx,Ly,Nbeta)
    integer, intent(in) :: t
    real :: W
    integer :: x,y,xp,yp,tp,xm,ym,tm
    logical :: same
    real :: s, cdm, sdm, pot

    cdm = cosh(delta*mu)
    sdm = sinh(delta*mu)

    W = 0

    do x = 1,Lx
        do y = 1,Ly
            pot = 0

            xp = mod(x,Lx)+1
            yp = mod(y,Ly)+1
            tp = mod(t,Nbeta)+1

            xm = mod(x-2+Lx,Lx)+1
            ym = mod(y-2+Ly,Ly)+1
            tm = mod(t-2+Nbeta,Nbeta)+1

            if (xp /= x) then
                same = lattice(x,y,t) .eqv. lattice(xp,y,t)
                pot = pot - spin(same)
                W = W - spin(same) * cos(2.*PI/Lx * (x+0.5))
            end if

            if (yp /= y) then
                same = lattice(x,y,t) .eqv. lattice(x,yp,t)
                pot = pot - spin(same)
                W = W - spin(same) * cos(2.*PI/Lx * x)
            end if

            if (xm /= x) then
                same = lattice(x,y,t) .eqv. lattice(xm,y,t)
                pot = pot - spin(same)
            end if

            if (ym /= y) then
                same = lattice(x,y,t) .eqv. lattice(x,ym,t)
                pot = pot - spin(same)
            end if

            if (lattice(x,y,t) .eqv. lattice(x,y,tp)) then
                W = W - mu*sdm/cdm * exp(delta*pot) * cos(2.*PI/Lx * x)
            else
                W = W - mu*cdm/sdm * exp(delta*pot) * cos(2.*PI/Lx * x)
            end if
        end do
    end do
end function

subroutine measure(lattice, sample)
    logical, intent(in) :: lattice(Lx,Ly,Nbeta)
    real, intent(out) :: sample(Nbeta)
    integer :: t, tp, dt
    real :: H, W(Nbeta), Wcor(Nbeta-1)

    H = 0
    W = 0
    Wcor = 0

    do t = 1,Nbeta
        H = H + hamiltonian(lattice,t)
        W(t) = wave(lattice,t)
    end do
    H = H/Nbeta

    do t = 1,Nbeta
        do tp = 1,Nbeta
            dt = mod(t-tp+Nbeta,Nbeta)
            if (dt > 0) then
                Wcor(dt) = Wcor(dt) + W(t)*W(tp)
            end if
        end do
    end do
    Wcor = Wcor/Nbeta

    sample(1) = H
    sample(2:) = Wcor
    !print *, H, Wcor
end subroutine

end program
