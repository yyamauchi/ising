#!/usr/bin/env python

"""
Producing an effective viscosity plot.
"""

import numpy as np
from scipy.optimize import fsolve
import sys

PLOT = True

# Read data
dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])
nbeta = len(dat)
beta = dat[-1,0]+dat[1,0]

# The integration kernel.
def K(tau,omega):
    return np.cosh(omega*(beta/2-tau)) * np.exp(-omega*beta/2)

# The spectral function.
def rho_pure(omega, omega0, gamma):
    return 1/(np.pi * gamma) * gamma**2 / ((omega-omega0)**2+gamma**2)

def effective_mass(ntau):
    def C(tau,A,m):
        return A*np.cosh(m*(tau-beta/2))
    def f(x):
        A, m = x
        return [C(dat[ntau-1,0],A,m)-dat[ntau-1,1],C(dat[ntau+1,0],A,m)-dat[ntau+1,1]]
    A, m = fsolve(f,(1,1))
    return m

def effective_omega_gamma(ntau):
    def C(tau, A, omega, gamma):
        # TODO
        return 0
    def f(x):
        A, omega, gamma = x
        def C_(tau):
            return C(tau, A, omega, gamma)
        dm, d0, dp = dat[ntau-1,:], dat[ntau,:], dat[ntau+1,:]
        return [C_(dm[0])-dm[1], C_(d0[0])-d0[1], C_(dp[0])-dp[1]]
    A, omega, gamma = fsolve(f,(1,1,0.1))
    return omega, gamma

def effective_omega(ntau):
    return effective_omega_gamma(ntau)[0]

def effective_gamma(ntau):
    return effective_omega_gamma(ntau)[1]

def effective_viscosity(ntau):
    ef_omega_0, ef_gamma = effective_omega_gamma(ntau)
    # TODO
    return 0


if PLOT:
    import matplotlib.pyplot as plt

    fig, axes = plt.subplots(2,2,tight_layout=True,figsize=(12.8,9.6))

    # Effective mass (should not converge)
    axes[0,0].plot(dat[2:-1,0], np.vectorize(effective_mass)(range(2,nbeta-1)))
    axes[0,0].set(xlabel='$\\tau$', ylabel='Effective mass')

    # Effective omega_0
    axes[0,1].plot(dat[2:-1,0], np.vectorize(effective_omega)(range(2,nbeta-1)))
    axes[0,1].set(xlabel='$\\tau$', ylabel='Effective $\\omega_0$')

    # Effective gamma
    axes[1,0].plot(dat[2:-1,0], np.vectorize(effective_gamma)(range(2,nbeta-1)))
    axes[1,0].set(xlabel='$\\tau$', ylabel='Effective $\\gamma$')

    # Effective dimensionless viscosity
    axes[1,1].plot(dat[2:-1,0], np.vectorize(effective_viscosity)(range(2,nbeta-1)))
    axes[1,1].set(xlabel='$\\tau$', ylabel='Effective $\\hat\\eta$')

    plt.show()
else:
    # TODO print
    pass
