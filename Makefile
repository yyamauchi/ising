FFLAGS = -O3
#FFLAGS = -O0 -g

FFLAGS += -fimplicit-none
FFLAGS += -fdefault-real-8
#PFLAG = -fprofile-generate=profiles/
#PFLAG = -fprofile-use=profiles/

all: thermal/thermal rt/rt comparison/iso comparison/metropolis comparison/brute rt_vis rt_2odr

thermal/thermal: thermal/thermal.f90
	gfortran ${FFLAGS} ${PFLAG} -o $@ $<

comparison/brute: comparison/brute.f90
	gfortran ${FFLAGS} -o $@ $<

comparison/metropolis: comparison/metropolis.f90
	gfortran ${FFLAGS} -o $@ $<

comparison/iso: comparison/iso.f90
	gfortran ${FFLAGS} -o $@ $<

rt_vis: rt_vis.f90
	gfortran ${FFLAGS} -o $@ $<

rt_2odr: rt_2odr.f90
	gfortran ${FFLAGS} -o $@ $<	

rt/rt: rt/rt.f90
	gfortran ${FFLAGS} -o $@ $<

clean:
	${RM} thermal/thermal rt/rt comparison/iso comparison/brute comparison/metropolis rt_vis rt_2odr
