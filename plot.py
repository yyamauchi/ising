#!/usr/bin/env python

import sys

import numpy as np

def bootstrap(x, N=100, B=50):
    if len(x) < B:
        B = len(x)
    Bs = len(x)//B
    y = np.array([np.mean(x[b*Bs:(b+1)*Bs]) for b in range(B)])
    m = np.mean(y)
    ms = []
    for n in range(N):
        s = np.random.choice(range(len(y)), len(y))
        ms.append(np.mean(y[s]))
    return m, np.std(ms)

dat = np.array([[float(x) for x in l.split()] for l in sys.stdin.readlines()])

cor = []
err = []
for i in range(dat.shape[1])[1:]:
    v, e = bootstrap(dat[:,i])
    cor.append(v)
    err.append(e)

import matplotlib.pyplot as plt

plt.errorbar(range(len(cor)), cor, yerr=err)
plt.show()
